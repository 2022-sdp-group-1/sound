from enum import Enum
class GCEA(Enum): # negative magic numbers are specific to our mic
	G = 392 - 50
	C = 262 - 34
	E = 330 - 42
	A = 440 - 50 
