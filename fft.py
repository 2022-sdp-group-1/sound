#%%
import os
import wave
import numpy as np
import matplotlib.pyplot as plt
from savitzky_golay import *

def read_sound_file(dir : str):

	audiofile = wave.open(dir)
	
	sample_rate = audiofile.getframerate() # sample rate
	frames = audiofile.getnframes()
	duration = frames / float(sample_rate)

	T = 1/sample_rate # sampling period
	t = duration # seconds of sampling
	N = frames # total points in signal

	# signal information
	signal = audiofile.readframes(-1)
	# use int32 for pi mic
	signal = np.frombuffer(signal, dtype ="int16")
	# signal = np.frombuffer(signal, dtype ="int16")
	time = np.linspace( 0, len(signal) / sample_rate, num = len(signal))

	return (duration, time, signal, N, sample_rate)

def fft(signal, N, sample_rate, discard_freq_above=1000, discard_freq_below = 50):
	pass

	Y_k = np.fft.fft(signal)[0:int(N/2)]/N # FFT function from numpy
	Y_k[1:] = 2*Y_k[1:] # need to take the single-sided spectrum only
	Pxx = np.abs(Y_k) # be sure to get rid of imaginary part

	f = sample_rate*np.arange((N/2))/N; # frequency vector

	f = f[f<discard_freq_above]
	Pxx = Pxx[0:len(f)]

	f = f[f>discard_freq_below]
	Pxx = Pxx[-len(f):]
	return (f,Pxx)
	
def fft_savitzky_golay(signal, N, sample_rate, discard_freq_above=1000):
	f, Pxx = fft(signal, N, sample_rate, discard_freq_above=discard_freq_above)
	PxxSG = savitzky_golay(Pxx, 51, 0)
	return (f, PxxSG)

def draw_fft(f,Pxx, log_scale=False, save_output=False, output_dir=None):
	fig,ax = plt.subplots()
	if log_scale:
		ax.set_xscale('log')
		ax.set_yscale('log')
	plt.plot(f,Pxx,linewidth=1)
	plt.ylabel('Amplitude')
	plt.xlabel('Frequency [Hz]')
	if save_output:
		plt.savefig(output_dir,dpi=300)  
	plt.show()

def get_max_amp(f, Pxx, count=1):
	# # large_amplitudes = np.sort(Pxx)[len(Pxx)-1-count : len(Pxx)-1]
	# # large_freqs = [ int(f[np.where(Pxx == a)[0]][0]) for a in large_amplitudes]
	# max_freq = np.zeros(3)
	# max_amp = np.zeros(3)
	# for i in range(len(Pxx)):
	# 	if Pxx[i] > max_amp[0]:
	# 		max_freq[0] = f[i]
	# 		max_amp[0] = Pxx[i]
	# 	elif Pxx[i] > max_amp[1]:
	# 		max_freq[1] = f[i]
	# 		max_amp[1] = Pxx[i]
	# 	elif Pxx[i] > max_amp[2]:
	# 		max_freq[2] = f[i]
	# 		max_amp[2] = Pxx[i]
	# return min(max_freq)
	# From the top three loudest frequencies, pick the lowest
    return min(sorted(zip(Pxx, f), reverse=True)[:3], key = lambda t:t[1])[1]

def draw_amp(time, signal, save_output=False, output_dir=None):
	plt.xlabel('Time (seconds)')
	plt.ylabel('Amplitude')
	plt.plot(time, signal)
	if save_output:
		plt.savefig(output_dir)  
	plt.show() 

#%%
def main(dir):
	duration, time, signal, N, sample_rate = read_sound_file(dir+'.wav' )
	f, Pxx = fft(signal, N, sample_rate)
	fSG, PxxSG = fft_savitzky_golay(signal, N, sample_rate)
	print((get_max_amp(f, Pxx, 3)))
	#print(get_max_amp(f, PxxSG, 3))
	# draw_amp(time, signal,)
	draw_fft(f, Pxx, save_output=False, output_dir=dir+'.png')
	draw_fft(fSG, PxxSG, save_output=False, output_dir=dir+'.png')
	pass

A_std_dir="./sound_file/A_short"
C_std_dir="./sound_file/C_short"

empty_dir="./sound_file/empty"

sf_dir = './sound_file/'

# (folder_name, count start from 1)
A_detuned_flat_1 = ('A_detuned_flat_1',9)
A_detuned_flat_2 = ('A_detuned_flat_2',7)
A_detuned_sharp_1 = ('A_detuned_sharp_1',6)
C_detuned_flat_1 = ('C_detuned_flat_1',9)
C_detuned_flat_2 = ('C_detuned_flat_2',6)
C_tuned_2 = ('C_tuned_2',5)

# Targe

# main(os.path.join(sf_dir, C_tuned_2[0], str(2)))
main(C_std_dir)
exit()

# main("/home/aoiduo/Downloads/wav/arecord_noisy/C_detuned_0")
selected_dir = C_tuned_2
for x in range(selected_dir[1]):
	print(os.path.join(sf_dir, selected_dir[0], str(x)), end='.wav ')
	main(os.path.join(sf_dir, selected_dir[0], str(x)))
