#%%
import os
import wave
import numpy as np
from utils import benchmark
from utils import UnKnown

def read_sound_file(dir : str):

	audiofile = wave.open(dir)
	
	sample_rate = audiofile.getframerate() # sample rate
	frames = audiofile.getnframes()
	duration = frames / float(sample_rate)

	T = 1/sample_rate # sampling period
	t = duration # seconds of sampling
	N = frames # total points in signal

	# signal information
	signal = audiofile.readframes(-1)
	signal = np.frombuffer(signal, dtype ="int32")
	time = np.linspace( 0, len(signal) / sample_rate, num = len(signal))

	return (duration, signal, N, sample_rate)

def fft(signal, N, sample_rate, discard_freq_above=1000, discard_freq_below = 50):

	# Y_k = benchmark(np.fft.fft,(signal,), label="np.fft")
	Y_k = np.fft.fft(signal)
	Y_k = Y_k[0:int(N/2)]/N # FFT function from numpy
	Y_k[1:] = 2*Y_k[1:] # need to take the single-sided spectrum only
	Pxx = np.abs(Y_k) # be sure to get rid of imaginary part

	f = sample_rate*np.arange((N/2))/N; # frequency vector
	
	f = f[f<discard_freq_above]
	Pxx = Pxx[0:len(f)]

	f = f[f>discard_freq_below]
	Pxx = Pxx[-len(f):]
	return (f,Pxx)
	
def get_max_amp(f, Pxx, count=5):
	min_amp_f = sorted(zip(Pxx, f), reverse=True)[:count]
	amp = min(min_amp_f, key = lambda t:t[1])[0]
	print([(round(a), b) for a,b in min_amp_f])

	if (amp < 1000000): # remove background 
		return UnKnown()
	f = min(min_amp_f, key = lambda t:t[1])[1]
	return f


#%%

if __name__ == "__main__":
	sf_dir = './sound_file/'

	# (folder_name, count start from 1)
	A_detuned_flat_1 = ('A_detuned_flat_1',9)
	A_detuned_flat_2 = ('A_detuned_flat_2',7)
	A_detuned_sharp_1 = ('A_detuned_sharp_1',6)
	C_detuned_flat_1 = ('C_detuned_flat_1',9)
	C_detuned_flat_2 = ('C_detuned_flat_2',6)
	C_tuned_2 = ('C_tuned_2',5)
