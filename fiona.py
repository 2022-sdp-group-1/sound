import threading
import tkinter as tk
from tkinter import BOTTOM, Canvas, NW
import time
from PIL import Image, ImageTk

from tune import Tuner, UnKnown
from motors import Motor
from GCEA import GCEA

motor = Motor()
tuner = Tuner(motor, GCEA.C)


def thread_function():
    while True:
        diff = tuner.tune()
        if isinstance(diff, UnKnown):
            diff = "Unknown()"
        
        strvar.set(f"{diff}\n")
        window.update()



global images

images = [
"./res/g_string.png",
"./res/c_string.png",
"./res/e_string.png",
"./res/a_string.png"
]

backgrounds = [
    "./res/SDP_Home.png",
    "./res/all_strings.png",
    "./res/SDP_End.png"
]


def show_frame(frame):
    frame.tkraise()

def chosen_note(panel,note):
    img = tk.PhotoImage(file = note)
    panel.configure(image = img)
    panel.image = img

    if (note == "./res/c_string.png"):
        x = threading.Thread(target=thread_function)
        x.start()
    

global count
count = 0
def change_string(panel):
    global count
    if count < 4:

        chosen_note(panel,images[count])
        count +=1

        frame.after(1000,change_string(panel))


window=tk.Tk()
window.title('ukulele')
window.attributes("-fullscreen", True)  
#window.state('zoomed')
window.minsize(240,330)
window.maxsize(240,330)

window.rowconfigure(0,weight=1)
window.columnconfigure(0,weight=1)













frame1 = tk.Frame(window)
frame2 = tk.Frame(window)
frame3 = tk.Frame(window)

for frame in (frame1,frame2,frame3):
    frame.grid(row=0,column=0,sticky='nsew')


#frame1 code
frame1_image = tk.PhotoImage(file = "./res/SDP_Home.png")
frame1_background = tk.Label(frame1, image = frame1_image)
frame1_background.configure(image = frame1_image)
frame1_background.place(x=0,y=0)

btn1 = tk.PhotoImage(file = "./res/start_button.png")

frame1_btn=tk.Button(frame1,image = btn1, borderwidth=0,command=lambda:show_frame(frame2))
frame1_btn.pack(side=BOTTOM)
frame1_btn.place(y = 250, x = 70)




#frame2 code
frame2_image = tk.PhotoImage(file = "./res/all_strings.png")
frame2_background = tk.Label(frame2, image = frame2_image)
frame2_background.configure(image = frame2_image)
frame2_background.place(x=0,y=0)

strvar = tk.StringVar()
tk.Label(frame2, textvariable=strvar, anchor='w').pack(fill='both')
strvar.set('')


btn2 = tk.PhotoImage(file = "./res/Button_design.png")

frame2_btn=tk.Button(frame2,image = btn2,border = 0,\
     borderwidth=0,\
        command=lambda:[show_frame(frame3),\
        chosen_note(frame2_background, "./res/all_strings.png")])
frame2_btn.pack(side=BOTTOM)
frame2_btn.place(y = 270, x = 70)

# tune_all_btn = tk.Button(frame2, image = btn2, command=change_string(frame2_background))
# tune_all_btn.place(y=30,x=23)

def xxxxxx():
    chosen_note(frame2_background,"./res/c_string.png")

btn_c = ImageTk.PhotoImage(file = "./res/c_button.png")
frame2_btn_c=tk.Button(frame2,image = btn_c,border = 0, borderwidth=0,command=xxxxxx)
frame2_btn_c.place(y = 103, x = 25)


btn_e = tk.PhotoImage(file = "./res/e_button.png")
frame2_btn_e=tk.Button(frame2,image = btn_e,border = 0,command=lambda:chosen_note(frame2_background,"./res/e_string.png"))
frame2_btn_e.place(y = 103, x = 170)

btn_g = tk.PhotoImage(file = "./res/g_button.png")
frame2_btn_g=tk.Button(frame2,image = btn_g,border = 0, borderwidth=0,command=lambda:chosen_note(frame2_background,"./res/g_string.png"))
frame2_btn_g.place(y = 170, x = 25)

btn_a = tk.PhotoImage(file = "./res/a_button.png")
frame2_btn_a=tk.Button(frame2,image = btn_a,border = 0, borderwidth=0,command=lambda:chosen_note(frame2_background,"./res/a_string.png"))
frame2_btn_a.place(y = 170, x = 170)


#frame3 code
frame3_image = tk.PhotoImage(file = "./res/SDP_End.png")
frame3_background = tk.Label(frame3, image = frame3_image)
frame3_background.place(x=0,y=0)

btn3 = tk.PhotoImage(file = "./res/SDP_End__button.png")

frame3_btn=tk.Button(frame3,image = btn3, borderwidth=0,command=lambda:show_frame(frame1))
frame3_btn.pack(side=BOTTOM)
frame3_btn.place(y = 220, x = 60)


show_frame(frame1)

window.mainloop()
