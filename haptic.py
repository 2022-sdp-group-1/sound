import board
import busio
import adafruit_drv2605
import threading

i2c = busio.I2C(board.SCL,board.SDA)
drv = adafruit_drv2605.DRV2605(i2c)
drv.sequence[0] = adafruit_drv2605.Effect(47)

def viberate():
	t = threading.Thread(target=lambda:[ drv.play() for _ in range(2000)])
	t.start()
