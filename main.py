import threading
import tkinter as tk
from PIL import Image, ImageTk
from tkinter import BOTTOM
import os, time
from typing import List
from main_utils import *
# from haptic import viberate

if os.path.basename(os.getcwd()) == 'sound':
    pass
from tune import Tuner, UnKnown
from motors import Motor
from GCEA import GCEA

t = None
frame_2_button_is_exit = False
motor = Motor()

window=tk.Tk()
window.title('ukulele')
window.attributes("-fullscreen", True)
window.minsize(240,320)
window.maxsize(240,320)
window.rowconfigure(0,weight=1)
window.columnconfigure(0,weight=1)


######################### MAKE FRAMES #########################

frames : List[tk.Frame] = []
for i in range(0,3):
    frame = tk.Frame(window)
    frame.grid(row=0,column=0,sticky='nsew')
    frames.append(frame)

frames_param = [\
    (frames[0],  "./res/SDP_Home.png",     "./res/start_button.png",      (250,70) , frames[1].tkraise),\
    (frames[1],  "./res/all_strings.png",  "./res/tune_all_button.png",   (260,70) , lambda:frame_2_button_function()    ),\
    (frames[2],  "./res/SDP_End.png",      "./res/start_again_button.png",   (220,70) , frames[0].tkraise        ) ]

def frame_2_button_function():
    global t 
    if frame_2_button_is_exit: 
        t.__stop__ = True
    else: 
        print("tune all")
        t = threading.Thread(target=tune_all_thread_func)
        t.__stop__ = False
        t.start()

for x in frames_param:
    frame_maker(*x)
frame_2_maker(frames[1], frames[2])

frame2 = frames[1]


######################### ARROW #########################

arrow_canvas = tk.Canvas(frame2, highlightthickness=0, width=16, height=12, bg="white")
arrow_canvas.pack()
arrow_canvas.place(x = ARROW_CENTER_X, y = 65)
arrow_image = ImageTk.PhotoImage(Image.open('./res/tunometer_arrow.png'))
arrow_canvas.create_image(8, 6, anchor=tk.CENTER, image=arrow_image)

######################### FRAME 2 BUTTONS #########################

def tune_single_string_btn_click(tune):
    # launch thread
    global t
    print(tune)
    t = threading.Thread(target=lambda:[tuning_start(tune), tuning_single_string(tune), tuning_exit()])
    t.__stop__ = False
    t.start()

tune_individual_btns = {}
tune_individual_btns_img = {}
tune_individual_btns_location = {
    GCEA.C: (100, 25), GCEA.E: (100, 170), GCEA.G: (167, 25), GCEA.A: (167, 170)
}

for x in GCEA:
    dir = "./res/{}_button2.png".format(x.name.lower())
    tune_individual_btns_img[x] = tk.PhotoImage(file = dir)
    cmd = lambda xx = x: tune_single_string_btn_click(xx)
    tune_individual_btns[x] = tk.Button(frame2,image = tune_individual_btns_img[x], command=cmd)
    loc = tune_individual_btns_location[x] 
    tune_individual_btns[x].place(y = loc[0], x = loc[1])

txt_log_str_var = tk.StringVar()
txt_log_str_var.set('Pick a string to start')
txt_log_label = tk.Label(frame2, textvariable=txt_log_str_var, anchor='w')
txt_log_label.place(y=150,x=0, anchor="w")
txt_log_label.pack(fill='both')


def tune_all_thread_func():
    for x in [GCEA.G, GCEA.C, GCEA.E, GCEA.A,]:
        print(x)
        tuning_start(x)
        tuning_single_string(x)
        if t.__stop__:
            break
    if not t.__stop__:
        frames[2].tkraise()
    tuning_exit()
    print("unimplemented")


def tuning_start(tune):
    global frame_2_button_is_exit

    # change background
    dir = "./res/{}_string.png".format(tune.name.lower())
    img = tk.PhotoImage(file = dir)
    frame2.frame_background.configure(image = img)
    frame2.frame_background.image = img
    
    # disable GACE buttons
    for key, btn in tune_individual_btns.items():
        if key != tune:
            btn['state'] = tk.DISABLED
        else:
            btn['state'] = tk.NORMAL
            btn['command'] = 0
    
    # make frame 2 main button do exit function
    # frame2.frame_btn.configure(image = frame2.frame.stop_btn_img)
    frame2.frame_btn['image'] = frame2.stop_btn_img
    frame_2_button_is_exit = True

def tuning_single_string(tune):

    txt_log_str_var.set(f"Now tuning string {tune.name}")
    window.update()

    # record and rotate motor
    tuner = Tuner(motor, tune)
    while True:
        if t.__stop__ == True:
            frames[1].tkraise()
            print("exit")
            break

        # diff = tuner.tune()
        diff = tuner.dummy_tune()
        if isinstance(diff, UnKnown):
            txt_log_str_var.set("Not recognized")
            window.update()
            print(diff)
        elif diff == 0:
            txt_log_str_var.set("Done")
            window.update()
            print("done")
            time.sleep(1)
            arrow_canvas.place(x = ARROW_CENTER_X, y = 65)
            # viberate()
            time.sleep(1)
            break
        else:
            if diff > 0:
                txt_log_str_var.set("Too sharp")
            elif diff < 0:
                txt_log_str_var.set("Too flat")
                window.update()
            loc = convert_diff_to_arrow_location(diff, 0)
            print((diff, loc))
            arrow_canvas.place(x = loc, y = 65)

        
        

def tuning_exit():
    # enable GACE button
    for _, btn in tune_individual_btns.items():
        btn['state'] = tk.NORMAL

    for x in GCEA:
        cmd = lambda xx = x: tune_single_string_btn_click(xx)
        tune_individual_btns[x]['command'] = cmd

    # set frame 2 button function to tune_all
    global frame_2_button_is_exit
    frame_2_button_is_exit = False
    frame2.frame_btn['image'] = frame2.btn_img

    # change background back 
    img = tk.PhotoImage(file = "./res/all_strings.png")
    frame2.frame_background.configure(image = img)
    frame2.frame_background.image = img
    # reset arrow to center
    arrow_canvas.place(x = ARROW_CENTER_X, y = 65)
    # xxx
    txt_log_str_var.set("Done. ")
    window.update()




frames[0].tkraise()
window.mainloop()
