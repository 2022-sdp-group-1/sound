import tkinter as tk
from tune import UnKnown

ARROW_CENTER_X = 112.5
ARROW_LEFT_MOST = 15
ARROW_RIGHT_MOST = 210

def frame_maker(frame, background_img, btn_img, btn_location, btn_cmd ):
    frame.frame_image = tk.PhotoImage(file = background_img)
    frame.frame_background = tk.Label(frame, image = frame.frame_image)
    frame.frame_background.place(x=0,y=0)
    frame.btn_img = tk.PhotoImage(file = btn_img)
    frame.frame_btn=tk.Button(frame, image = frame.btn_img, borderwidth=0,command=btn_cmd)
    frame.frame_btn.pack(side=tk.BOTTOM)
    frame.frame_btn.place(y = btn_location[0], x = btn_location[1])

def frame_2_maker(frame, end_page):
    pass
    # exit_cmd = end_page.tkraise
    # btn_location = (260,140)
    # btn_img = "./res/Button_design.png"
    # frame.exit_btn_img = tk.PhotoImage(file = btn_img)
    # frame.exit_btn=tk.Button(frame, image = frame.exit_btn_img, borderwidth=0,command=exit_cmd)
    # frame.exit_btn.pack(side=tk.BOTTOM)
    # frame.exit_btn.place(y = btn_location[0], x = btn_location[1])

    stop_btn_img = "./res/stop_button_red.png"
    frame.stop_btn_img = tk.PhotoImage(file = stop_btn_img)


def convert_diff_to_arrow_location(diff, prev_loc):
    if isinstance(diff, UnKnown ):
        return prev_loc
    else:
        min_bound = -40
        max_bound = 40
        if diff < min_bound: return ARROW_LEFT_MOST
        elif diff > max_bound: return ARROW_RIGHT_MOST
        else: 
            m = (ARROW_RIGHT_MOST - ARROW_LEFT_MOST) / (max_bound - min_bound)
            return ARROW_CENTER_X + m * diff

