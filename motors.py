import RPi.GPIO as GPIO
import time
from utils import benchmark


# < 1 fast clockwise
# 1.42 center
# > 2 fast anti clockwise

class Motor:
    def __init__(self, servoPIN = 17, pwm = 250):
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(servoPIN, GPIO.OUT)
        self.p : GPIO.PWM = GPIO.PWM(servoPIN, pwm)
        self.p.start(0)
    pass

    def run(self, duty_cycle: int, duration: float):
        self.p.ChangeDutyCycle(duty_cycle)
        time.sleep(duration)
        self.p.ChangeDutyCycle(0) 
        '''
        https://forums.raspberrypi.com/viewtopic.php?t=277137
        use p.ChangeDutyCycle(0) instead of p.stop()
        '''

    def stop(self, duration: float):
        time.sleep(duration)

def motor_benchmark(motor: Motor, duty_cycle, duration, iteration):
    for i in range(0,iteration):
        print(f"iteration: {i}, expected duration = {duration}, actual duration: ", end='')
        benchmark(motor.run, (duty_cycle,duration))
        motor.stop(1)
