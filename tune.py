from fft_simple import * 
from GCEA import GCEA
from typing import Optional, Tuple
from motors import Motor
import sys, time, random
from utils import UnKnown

def compare(input: str, acge: GCEA) -> int:

    duration, signal, N, sample_rate = read_sound_file(input)
    f,pxx = fft(signal, N, sample_rate)
    max_amp = get_max_amp(f,pxx)
    if isinstance(max_amp , UnKnown):
        return UnKnown()
    else:
        return max_amp - acge.value

def compare_two_files(reference: str, input: str) -> int:

    raise Exception("deprecated")
    xs = [ read_sound_file(dir+'.wav') for dir in [reference, input]] 
    fpxx = []

    for x in xs:
        _, signal, N, sample_rate = x
        fpxx.append(fft(signal, N, sample_rate))
    
    reference = fpxx[0]
    input = fpxx[1]

    return get_max_amp(*reference) - get_max_amp(*input)


def get_duty_cycle(diff: int) -> Tuple[int,int]:
    '''
    diff > 0  ->  loosen string, rotate CW
    diff < 0  ->  tighten string, rotate CCW
    '''
    if diff > 20:
        duty_cycle = 50
    elif diff > 0:
        duty_cycle = 42.5
    elif diff < -20:
        duty_cycle = 20
    elif diff < 0:
        duty_cycle = 27.5
    else: 
        duty_cycle = 0
    return duty_cycle

def get_duration(diff: int, disable_long_rotation=False) -> Tuple[int,int]:
    if disable_long_rotation:
        duration = 0.5
    
    if diff > 50 or diff < -50 :
        duration = 1
    else: 
        duration = 0.5
    return duration

def arecord_wrapper(duration=2):
    file_name_template = '/dev/shm/{}f.wav'
    dir = file_name_template.format(str(time.time()))
    # pi zero
    # os.popen(f'arecord -D hw:1,0 -c 2 -d {duration} -r 48000 -f S32_LE -t wav -V mono -v {dir}  2>/dev/null')
    # pi 3b+
    os.popen(f'arecord -D hw:2,0 -c 2 -d {duration} -r 48000 -f S32_LE -t wav -V mono -v {dir}  2>/dev/null')
    time.sleep(duration)
    '''
    use system shell and to call arecord and save in /dev/shm
    '''
    return dir
    
class Tuner():

    def __init__(self, motor: Motor, target: int):
        self.is_close = False
        self.motor = motor
        self.target = target

    def dummy_tune(self):
        time.sleep(5)
        return 0
        time.sleep(random.randint(1,2))
        return random.randint(-1000, 1000)
    
    def what_is_this_thing(self, diff):
        if diff < -100:
            return UnKnown()
        elif diff > 100:
            return UnKnown()

    def tune(self, debug=False, offset = 3) :
        dir = arecord_wrapper(duration=1)
        diff = compare(dir, self.target)
        print(f"{diff} | {dir}") if debug == True else None
        os.remove(dir) if debug == False else None 

        if isinstance(diff, UnKnown):
            return UnKnown()
        elif abs(diff) < offset:
            return 0
        elif isinstance(self.what_is_this_thing(diff), UnKnown):
            return UnKnown()
        else:
            if abs(diff) < 50:
                self.is_close = True
            duty_cycle = get_duty_cycle(diff)
            duration = get_duration(diff, self.is_close)
            self.motor.run(duty_cycle, duration)
            return diff

    def tune_loop(self, debug=False):
        x = 114514
        while True:
            if x == 0:
                print("Successful")
                break
            else:
                x = self.tune(debug=debug)
                if isinstance(x, UnKnown):
                    pass

    def dummy_tune_loop(self, ):
        x = 114514
        while True:
            if x == 0:
                print("Successful")
                break
            else:
                x = self.dummy_tune()

if __name__ == "__main__":

    motor = Motor()
    tuner = Tuner(motor, GCEA.C)
    tuner.tune_loop(debug=True)

    # input_dir="./sound_file/C_detuned_flat_2/2.wav"
    # input_dir="./sound_file/G_short.wav"
    # while True:
    #     input("Press any key to continue...")
    #     main(use_file = False)
    #     print()
        # print("file")
        # main(use_file = True, input_dir=input())

# def main(use_file = True, input_dir = None):
#     if use_file:
#         dir = input_dir
#     else:
#         dir = arecord_wrapper()
#     print(dir)
#     diff = compare(dir, ACGE.C)
#     print(diff)
#     duty_cycle, duration = get_duty_cycle(diff)
#     motor = Motor()
#     motor.run(duty_cycle, duration)
