import time
import sys
from abc import ABC

def benchmark(func, params, label=None):
	start = time.time()
	ret = func(*params)
	end = time.time()
	if label != None:
		print(label, end=' ',)
	print(round(end - start,8), )
	return ret
	pass

class UnKnown(ABC):
    def __str__(self):
        return "UnKnown()"